# WhatsAppCards

Telegram bot prototype of  WhatsAppCards postcard generator

> "Send your loving Granny a postcard full of love and care"

Languages
---------
As this is only a demo, you can only create congratulations in Russian

Usage
------
1. Go to https://t.me/whatsup_card_bot
2. Send him some text:   
2.1. With your custom congratulations  
*OR*  
2.2 With theme of desired postcard (пока доступны Новый Годб Рождество, 8 Марта, Троица, Пасха и день рождения)
3. Enjoy a dedicated postcard and forward it to your Granny. Fill her heart with joy of love and care

Examples
--------
 - Custom congratulations and Themed poem  
<img src="misc/poetry.png" alt="Custom congratulations" width="480" height="360" border="10" /></a>
<img src="misc/custom.png" alt="Themed poem" width="480" height="360" border="10" /></a>

Structure
---------
 - *dev* - contains modules for text processing, image processing, NER and poem generation
 - *fonts* - contains fonts for congratulation texts
 - *images* - contain pre-made images for postcards
 - *install* - folder for application installation scripts
 - *log* - well... folder for logging files
 - *texts* - contains pre-made poetry from http://pozdravok.ru
 - *main.py* - is the main file that starts and operaits bot
 - *misc* - contains images for repository

Installation
------------
If you want to develop some additional functionality or test our application locally, please do the following:
!WARNING: there can be only one instance of telegram bot at a time so you have to change token at line 14 in main.py
1. clone repository
2. Navigate to WhatsappCards folder
3. Run library installation with (!WARNING: If some packages are still missing please install them manually):  
`pip install -r requirements.txt`
4. Download nltk packages by openig your command line and running:  
`python <path-to-whatsappcard-folder>/install/nltkmodules.py`
5. You are good to go, now run:
`python main.py`
6. Enjoy your own postcard bot

Further work
-------
- [ ] Smarter NER и Intent classification
- [ ] Animation and effects
- [ ] Music generation
- [ ] Image generation

Disclaimer
----------
Bot uses poetry from http://pozdravok.ru. This content is not ours and should not used for commercial use of any kind. It solely belongs to its authors.

