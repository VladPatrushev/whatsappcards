from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from dev.image_proc import congratulation_func
from dev.text_proc import *
import aiohttp
import asyncio
import logging

logging.basicConfig(format='%(asctime)s %(message)s',
                    filename='myapp.log',
                    level=logging.INFO)

bot = Bot(token="1292201111:AAFJfnuUJcB5sBLzZQVuTvBDexL3wTf_6xs")

dp = Dispatcher(bot)

@dp.message_handler(commands = ['start', 'help'])
async def send_film(message: types.Message):
    await message.reply(f"Привет! Этот бот предназначен для генерации \
                          открыток в Whats app")


@dp.message_handler(content_types = types.ContentType.TEXT)
async def quote_message_handler(message:types.Message):
    request_found = find_request(message.text)
    if request_found:
        congratulation_text = generate_text(message.text)
    else:
        congratulation_text = message.text
    image_to_send = congratulation_func(congratulation_text,
                                        request_found = request_found)

    await bot.send_photo(chat_id=message.chat.id, photo=image_to_send)


if __name__ == '__main__':
    logging.info('Bot: started')
    executor.start_polling(dp)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(quote_message_handler())
    logging.info('Bot: finished')
