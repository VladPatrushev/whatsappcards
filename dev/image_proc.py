from PIL import Image, ImageFont, ImageDraw
import random
import os
from io import BytesIO
import textwrap


def get_files(path: str, formats: list) -> list:
    all_files = os.listdir(path)
    files = [file for file in all_files if file.split(".")[1] in formats]
    return files


PATH_TO_FRONT_ELEMENTS = "images/front_elements"
PATH_TO_BACKGROUNDS = "images/flowers/tulip"
PATH_TO_FONTS = "./fonts"

FRONT = get_files(path=PATH_TO_FRONT_ELEMENTS, formats=["png", ])
BACKGROUNDS = get_files(path=PATH_TO_BACKGROUNDS, formats=["jpeg", "jpg"])
FONTS = get_files(path=PATH_TO_FONTS, formats=["ttf", "otf"])


def get_elements_for_picture() -> dict:
    return {"font": f"{PATH_TO_FONTS}/{random.sample(FONTS, k=1)[0]}",
            "front":  f"{PATH_TO_FRONT_ELEMENTS}/{random.sample(FRONT, k=1)[0]}",
            "background": f"{PATH_TO_BACKGROUNDS}/{random.sample(BACKGROUNDS, k=1)[0]}"
            }


def paste_front_elements(card_image, front_element):
    element_to_paste = Image.open(front_element).convert("RGBA")
    card_image.paste(element_to_paste, (30,30), element_to_paste)
    return card_image


def draw_text_on_image(card_image, congratulation_phrase, fontpath, color):
    font = ImageFont.truetype(font=fontpath, size=30)
    draw = ImageDraw.Draw(card_image)
    x, y = card_image.size
    w, h = draw.textsize(congratulation_phrase, font=font)
    draw.text(((x - w) / 2, (y - h) / 2), congratulation_phrase.upper(),
              font=font, fill=color, align="center")


def draw_poem_on_image(card_image, congratulation_phrase, fontpath, color):
    font = ImageFont.truetype(font=fontpath, size=25)
    draw = ImageDraw.Draw(card_image)
    x, y = card_image.size
    w, h = draw.textsize(congratulation_phrase, font=font)
    draw.text(((x - w) / 2 + 35, (y - h) / 2), congratulation_phrase.upper(), font=font, fill="yellow", align="right")

def congratulation_func(phrase: str,
                        request_found : bool = False):
    picture_ingredients = get_elements_for_picture()
    bg = Image.open(picture_ingredients["background"]).convert("RGBA")
    element = picture_ingredients["front"]
    font = picture_ingredients["font"]
    output_content = BytesIO()
    output_content.name = 'output_content.jpeg'

    card = Image.new(mode="RGB", size=bg.size)
    card.paste(bg, (0, 0, *bg.size), bg)
    card = paste_front_elements(card, element)

    if request_found:
        draw_poem_on_image(card_image=card, congratulation_phrase=phrase,
                           fontpath=font, color="yellow")
    else:
        draw_text_on_image(card_image=card, congratulation_phrase=phrase,
                           fontpath=font, color="yellow")


    card.save(output_content, 'JPEG')
    output_content.seek(0)
    return output_content
