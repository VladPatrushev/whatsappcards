"""
DISCLAIMER : code uses poetry from http://pozdravok.ru. This content is not used for commercial use of any kind and solely belongs to it authors.
"""
import json
import random
from pathlib import Path, PurePath
import logging

def get_themed_poem(theme : str,
                    poem_json = PurePath(Path().absolute(),'texts/poetry.json')):
    """
    Returns random poem by theme
    Imput:
    theme, str - one of ['рождество', 'новый год', 'пасха', 'день рождения',
                         'троица', 'март', 'юбилей']
    Output:
    poem, str - randomly chosen themed poem
    """
    # Opening JSON file
    with open(poem_json, 'r', encoding='utf-8') as json_file:
        data = json.load(json_file)
        theme_list = list(data.keys())
        if theme in theme_list:
            key = random.choice(list(data[theme].keys()))
            logging.info('get_themed_poem: Theme : {}, \
                          Chosen key {}')#.format(theme, key))
            return data[theme][key] # get random poem
        else:
            return None

if __name__ == "__main__":
    get_themed_poem('рождество')
