import nltk
from nltk.corpus import stopwords
import pymorphy2
import string
import logging

from dev.intent_extraction import extract_intent
from dev.poem_generator import get_themed_poem

stopwords = set(stopwords.words("russian"))
morph = pymorphy2.MorphAnalyzer()

def text_lower(text : str):
    """ text to lowercase """
    return text.lower()

def remove_punctuation(text : str):
    """ remove punctuation """
    translator = str.maketrans('', '', string.punctuation)
    return text.translate(translator)

# remove outlayer numbers
def remove_outlayer_number(text : str):
      if len(text) == 8 or len(text) == 6:
          # объединяем дату рождения
          text = 'birth:' + '.'.join([text[:2],text[2:4],text[4:]])

      # проверяем год ли рождения
      elif text.startswith('19'):
          text = 'birth:'+ text
      else:
        text = float(text)
        if text >= 0. and text <= 31.0:
            # скорее всего это праздник вроде 8 марта
            text= 'date:' + str(int(text))
        elif text >= 31. and text <= 110.:
            text= 'birth:'+ str(int(text))
        else:
            text = 'ERR'

      return text

def preprocess_text(text : str):
    '''
    Preprocesses text:
    lowercase, tokenize, punctuation deletion, strange date deletion,
    overall cleaning

    Input:
    text, str - input text

    Returns:
    list of lists of strings - returns list of lists of tokens
    '''
    preprocessed_text = []
    # all to lowercase
    text = text_lower(text)

    # sentence tokenize text
    sent_list = nltk.sent_tokenize(text, language="russian")

    for sent in sent_list:

        #get rid of punctuation
        sent = remove_punctuation(sent)

        # get
        token_list = nltk.word_tokenize(sent, language='russian')
        morphed_list = [morph.parse(token)[0].normal_form for token \
                        in token_list if token not in stopwords
                       ]

        # check for strange numbers and delete them
        filtered_list = [remove_outlayer_number(token) if token.isdigit() \
                         else token  for token in morphed_list]

        # #dumb check for years of birth
        # filtered_list = [token]
        if 'ERR' in filtered_list: filtered_list.remove('ERR')
        preprocessed_text.append(filtered_list)

    return preprocessed_text

def find_request(text: str):
    if text.lower().startswith("поздравь"):
        logging.info('find_request: Found request')
        return True
    logging.info('find_request: Request not found')
    return False

def generate_text(request : str):
    prep_request = preprocess_text(request)
    logging.info('generate_text: Prepared request')

    # extract intent and make intent dict
    intent_dict = extract_intent(prep_request)
    logging.info('generate_text: Created intent_dict')
    print(intent_dict)
    if 'theme' in intent_dict:
        logging.info('generate_text: Found theme in intent_dict',
                     intent_dict['theme'])

        # get poetry according to texts
        output_text = get_themed_poem(intent_dict['theme'])

    else:
        output_text = 'C ' + request.lower().split('с ',1)[1] + ' !'
        logging.info('generate_text: theme not found ', output_text)

    return output_text
