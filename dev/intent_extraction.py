def extract_intent(sent_list : list):
    '''
    Creates dict of intents. As a baseline

    Input:
    text is an list of list of strings

    Returns:
    intent_dict, dict

    intent_dict structure:
          {
            name: str, default:"бабушка"
            theme: str (one of ['пасха', 'новый год', 'троица',
                                  'день рождения', '8 марта', 'рождество',
                                  'юбилей'])  # TODO ADD MORE HOLIDAYS
            birth: str,
            date: str,
          }
    '''
    intent_dict = {}
    holiday_list = ['пасха', 'новый', 'троица', 'рождение',
                    'март', 'рождество', 'юбилей']
    intent_dict['name'] = 'бабушка'

    for sentence in sent_list:
        for token in sentence:
            if token in holiday_list:
                if token == "рождение":
                  token = "день рождения"
                if token == "новый":
                  token = token+ " год"
                intent_dict['theme'] = token
            if token.startswith('birth'):
                intent_dict['birth'] = token.split(':')[1]
            if token.startswith('date'):
                intent_dict['date'] = token.split(':')[1]

    return intent_dict
